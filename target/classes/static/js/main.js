$(document).ready(function () {

    function disableBack() { window.history.forward() }
    window.onload = disableBack();
    window.onpageshow = function(evt) { if (evt.persisted) disableBack() }

    $("#login-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit() {

    var jsonData = {}
    jsonData["email"] = $("#username").val();
    jsonData["password"] = $("#password").val();

    /*var formData = {
                username : $("#username").val(),
                password :  $("#password").val()
            }*/

    $("#btn-login").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "api/home/login",
        data: JSON.stringify(jsonData),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            if(data.status == "success"){
                console.log("SUCCESS : ", data);
                Cookies.set("data", JSON.stringify(data));
                window.location.href = "welcome";
            }else if(data.status == "failure"){
                console.log("ERROR : ", data);
                $("#errorDiv").html("<strong> Invalid Credentials </strong>");
            }

            $("#btn-search").prop("disabled", false);

        },
        error: function (e) {
            $("#errorDiv").html("<strong> Invalid Input </strong>");

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });

}