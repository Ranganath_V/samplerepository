package com.example.demo.repository;

import com.example.demo.model.Employee;
import com.example.demo.model.Timesheet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by hashworks-60 on 22/5/17.
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findById(Long empId);
}
