package com.example.demo.repository;

import com.example.demo.model.Employee;
import com.example.demo.model.Timesheet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hashworks-60 on 19/5/17.
 */
@Repository
public interface TimesheetRepository extends JpaRepository<Timesheet, Long>
{
    List<Timesheet> findAllByEmployee(Employee employee);
}
