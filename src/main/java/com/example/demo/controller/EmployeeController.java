package com.example.demo.controller;

import com.example.demo.model.Employee;
import com.example.demo.model.Timesheet;
import com.example.demo.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.*;

/**
 * Created by hashworks-60 on 19/5/17.
 */
@Slf4j
@RestController
@RequestMapping(value = "/api/timesheet/employee/")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "view/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> viewTimesheet(@PathVariable long id) {

        Map responseMap = new HashMap();
        HttpStatus status = HttpStatus.OK;

        try {
            responseMap.put("message", "saved successfull");
            responseMap.put("timesheet", this.employeeService.viewTimesheet(id));
            return new ResponseEntity<Object>(responseMap, status);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            status = HttpStatus.INTERNAL_SERVER_ERROR;
            responseMap.put("message", "failed to save");
            return new ResponseEntity<Object>(responseMap, status);
        }
    }
    @RequestMapping(value="save", method = RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addEmployee(@RequestBody  Employee employeeSetup)
    {
        if(employeeSetup!= null)
        {
            if (employeeSetup.getEmployee_name() != null)
            {
                log.info("Creating Employee : "+ employeeSetup.getEmployee_name());
                return new ResponseEntity<Object>(this.employeeService.addEmployee(employeeSetup), HttpStatus.OK);

            }
            else
            {
                log.error("Name is mandatory");
                return new ResponseEntity<Object>(HttpStatus.NOT_ACCEPTABLE);
            }
        }

        else
        {
            log.error("Failed to creating Employee");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
