package com.example.demo.controller;

import com.example.demo.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by abhishek on 26/5/17.
 */
@RestController
@Slf4j
@RequestMapping(value = "/api/home/")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> loginAuthentication(@RequestBody String authenticationSetup) {
        try {
            log.info("logged in successfully");
            return new ResponseEntity<Object>(this.authenticationService.loginAuthentication(authenticationSetup), HttpStatus.OK);
        } catch (Exception e) {
            log.info("failed to logged in", e);
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
