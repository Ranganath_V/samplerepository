package com.example.demo.controller;

import com.example.demo.service.ListScmApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tapan on 22/09/2017.
 */

@RestController

public class BranchListController {

    @Autowired
    private ListScmApiService listScmApiService;

    @ResponseBody
    public ResponseEntity<List> getBranchesApi(){
        HttpStatus status = HttpStatus.OK;
        List responseList = new ArrayList();
        responseList = listScmApiService.getBranchesApi();
//        log.info("API list "+responseList);
        return new ResponseEntity<List>(responseList,status);

    }
}
