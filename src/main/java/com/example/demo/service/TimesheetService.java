package com.example.demo.service;

import com.example.demo.model.Timesheet;

import javax.transaction.Transactional;
import java.lang.reflect.Method;

/**
 * Created by hashworks-60 on 19/5/17.
 */
@Transactional
public interface TimesheetService {
    public boolean submitTimesheet (String timesheet);
}
