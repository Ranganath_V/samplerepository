package com.example.demo.service.bean;

import com.example.demo.model.Timesheet;
import com.example.demo.repository.ManagerRepository;
import com.example.demo.repository.TimesheetRepository;
import com.example.demo.service.ManagerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.StyleSheet;
import java.util.Date;

/**
 * Created by hashworks-60 on 22/5/17.
 */
@Slf4j
@Service
public class ManagerBean implements ManagerService{

    @Autowired
    private ManagerRepository managerRepository;

    @Autowired
    private TimesheetRepository timesheetRepository;

    public boolean approveTimesheet(long id)
    {
        try
        {
            Timesheet timesheet=  this.timesheetRepository.findOne(id);
            timesheet.setStatus(true);
            timesheet.setUpdatedDate(new Date());
            timesheetRepository.save(timesheet);

            return true;
        }
    catch (Exception e)
    {
        return false;
    }

    }
}
