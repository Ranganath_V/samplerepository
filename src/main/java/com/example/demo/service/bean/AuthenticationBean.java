/*package com.example.demo.service.bean;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abhishek on 26/5/17.
 */
/*@Service
@Slf4j
public class AuthenticationBean implements AuthenticationService {

        @Autowired
        private UserRepository userRepository;

        public Map loginAuthentication(String authenticationSetup) {
            Map data = new HashMap();
            JSONObject jsonObject = new JSONObject(authenticationSetup);
            Long userId = jsonObject.getLong("userId");
            if (userId != null && this.userRepository.exists(userId) ) {
                User user = this.userRepository.findOne(userId);
                //get password
                //compare to this password
                String password = jsonObject.getString("userPassword");
                if (password != null && user.getPassword().equals(password)) {
                    data.put("status", "success");
                    data.put("message", "loged in successfully");
                    return data;
                } else {
                    data.put("status", "failure");
                    data.put("message", "password miss-matching");
                    return data;
                }

            } else {
                data.put("status", "fail");
                data.put("message", "Failed to loged in ");
                return data;
            }
        }
}*/

package com.example.demo.service.bean;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class AuthenticationBean implements AuthenticationService {

    @Autowired
    private UserRepository userRepository;

    public Map loginAuthentication(String authenticationSetup) {
        Map data = new HashMap();
        JSONObject jsonObject = new JSONObject(authenticationSetup);
        Long userId = jsonObject.getLong("userId");
        if (userId != null && this.userRepository.exists(userId) ) {
            User user = this.userRepository.findOne(userId);
            //get password
            //compare to this password
            String password = jsonObject.getString("userPassword");
            if (password != null && user.getPassword().equals(password)) {
                data.put("status", "success");
                data.put("message", "loged in successfully");
                return data;
            } else {
                data.put("status", "failure");
                data.put("message", "password miss-matching");
                return data;
            }

        } else {
            data.put("status", "failure");
            data.put("message", "loged in failed");
            return data;
        }
    }
}
