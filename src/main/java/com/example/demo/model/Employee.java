package com.example.demo.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.apache.catalina.Manager;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by hashworks-60 on 17/5/17.
 */
@Entity
@Table(name="employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String employee_name;

    @Getter
    @Setter
    private String user_name;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private Enum roles;


    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manager_id")
    @JsonManagedReference
    private Employee managerApproval;


    @Getter
    @Setter
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
    private Set<Timesheet> timesheet ;
}